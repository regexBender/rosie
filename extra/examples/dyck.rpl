-- Jamie A. Jennings

-- CFG: Matches strings of properly balanced parentheses, brackets
-- https://en.wikipedia.org/wiki/Dyck_language

grammar 
   A = { "(" dyck? ")" }
   B = { "[" dyck? "]" }
   dyck = { A / B }*
in
   S = dyck $
end

-- Tests of PURE parentheses 
-- test S accepts "", "()", "()()", "(())", "(()())"
-- test S rejects "(", ")", ")(", "(()", "())", "())(", ")(()"

-- Tests of PURE square brackets
-- test S accepts "", "[]", "[][]", "[[]]", "[[][]]"
-- test S rejects "[", "]", "][", "[[]", "[]]", "[]][", "][[]"

-- Tests of MIXED parentheses and brackets
-- test S accepts "[]()", "()[]", "([])", "[()]"
-- test S accepts "[()]()", "([])[]", "[[[([])]]]", "([()])", "([()])([()])"
-- test S rejects "()]()", "([])[", "[[[([)]]]]", "[(()])", "[)", "(]"
-- test S rejects "a", "()a", "[]a"

-- -----------------------------------------------------------------------------
-- Explanation
-- -----------------------------------------------------------------------------

-- The grammar above has one rule for each kind of balanced character: 'A' for
-- parentheses (round brackets) and 'B' for square brackets.  More can be added.
--
-- The rule 'A' says that a valid Dyck string can be made by surrounding another
-- 'dyck' string with parentheses.  The expression 'dyck?' means "0 or 1 matches
-- of 'dyck'", which is what allows the rule 'A' to match "()", with no string
-- in between "(" and ")".a
--
-- The rule 'dyck' matches zero or more repetitions of 'A' or 'B'.
--
-- Finally, the rule 'S' matches 'dyck' followed by the end of the input.  This
-- is important in PEG languages, because a PEG match can succeed without
-- matching all of the input string.  Without the use of $ to match the end of
-- the input, 'dyck' would match "()abc" by matching "()" and leaving the 3
-- characters "abc" unmatched.


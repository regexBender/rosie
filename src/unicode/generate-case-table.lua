-- -*- Mode: Lua; -*-
--
-- unicode.lua
--
-- © Copyright Jamie A. Jennings 2018.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings

local rosie = require("rosie")
import = rosie.import
local list = import("list")
local violation = import("violation")


local engine = rosie.engine.new()
local ok, pkgname, errs = engine:loadfile("ucd.rpl")
if not ok then
   error(table.concat(list.map(violation.tostring, errs), "\n"))
end

ucd = dofile("ucd.lua")
enumerated = dofile("enumerated.lua")
util = dofile("util.lua")
-- unicode_rpl_compiler = dofile("unicode-rpl-compiler.lua")

UNICODE_VERSION = "10.0.0"
-- UCD_DIR = assert(rosie.env.ROSIE_HOME) .. "/src/unicode/UCD-" .. UNICODE_VERSION .. "/"
UCD_DIR = assert(rosie.env.ROSIE_HOME) .. "/../../../src/unicode/UCD-" .. UNICODE_VERSION .. "/"

UnicodeDataFile = "UnicodeData.txt"

filename = "CaseMapping2.bin"

local simple_case_table = {}

function create_simple_case_table(character_db, start_index, end_index)
    local simple_case_table = {}
    simple_case_table.size = 0

    for index = start_index, end_index do
        local match_list = {}
        local match_list_size = 0
        if character_db[index] then

            for j, case in ipairs({"lower", "upper", "title"}) do
                local case_codepoint = character_db[index][case]
                if case_codepoint ~= "" and case_codepoint ~= nil then
                    local case_codepoint_hex = tonumber(case_codepoint, 16)
                    match_list[case] = case_codepoint_hex
                    match_list_size = match_list_size + 1
                end
            end

            if match_list_size > 0 then
                local key = index
                simple_case_table[key] = match_list

                simple_case_table.size = simple_case_table.size + 1
            end

        end

    end
    print("simple_case_table.size = " .. simple_case_table.size)
    return simple_case_table
end



-- -----------------------------------------------------------------------------
-- Top level:  read(); write()
-- -----------------------------------------------------------------------------

function load_character_db()
    local character_db = nil
    optional_directory = optional_directory or "/Users/Alec/Dropbox/Education/NCSU/Courses/mscs/CSC630/rosie/src/unicode/tmp"

    print("Loading the data from UnicodeData.txt...")
    character_db = ucd.processUnicodeData(engine, UnicodeDataFile)

    return character_db
end

function create_case_mappings()
    local character_db = load_character_db()

    print("Creating the case mapping table...")
    simple_case_table = create_simple_case_table(character_db, 1, character_db.max_codepoint)
    -- full_case_table   = create_simple_case_table(case_folding_db)
    -- table.print(simple_case_table)
    optional_directory = optional_directory or "/Users/Alec/Dropbox/Education/NCSU/Courses/mscs/CSC630/rosie/src/unicode/tmp"
    local filename = (optional_directory .. "/test.lua")
    print("Writing " .. filename)

    util.print_table(simple_case_table, filename)

end

--
-- function test_all_chars(character_db, max_codepoint)
--     local unicode_char_rpl = unicode_rpl_compiler
--             .get_unicode_char_rpl(max_codepoint)
--
--     local e = engine
--
--     local function simple_escape(unicode_char)
--         return "\\" .. unicode_char
--     end
--
--     local escape_chars = {
--         ["\\"] = true,
--         ["\""] = true
--     }
--
--     local case_test_table = {}
--
--     character_db = unicode.get_unicode_db()
--     for index = 0, max_codepoint do
--         if index ~= 0 and index % 10000 == 0 then
--             print(tostring(index) .. " characters tested")
--         end
--
--         case_test_table[index] = {}
--
--         unicode_char = utf8.char(index)
--         if escape_chars[unicode_char] then
--             unicode_char = simple_escape(unicode_char)
--         end
--
--         -- -- io.write("\n" .. ustring.escape(unicode_char) )
--         -- p, errs = e:compile('ci:"' .. unicode_char .. '"')
--
--         -- p = unicode_char_rpl[index].p
--         -- errs = unicode_char_rpl[index].errs
--         p = unicode_char_rpl[index]
--         case_test_table[index]["p"] = {p = p, check = p}
--         -- check(p)
--         if p then
--            ok, m, leftover = e:match(p, unicode_char)
--            case_test_table[index]["self"] = {
--                ok = ok, m = m, leftover = leftover, check = ok and m
--            }
--            -- check(ok and m and (leftover==0), "m: " .. tostring(m) )
--
--            ok, m, leftover = e:match(p, "")
--            case_test_table[index]["empty"] = {
--                ok = ok, m = m, leftover = leftover, check = ok and not m
--            }
--            -- check(ok and not m)
--
--            ok, m, leftover = e:match(p, "\0")
--            if index == 0 then
--                case_test_table[index]["\0"] = {
--                    ok = ok, m = m, leftover = leftover, check = ok and m and (leftover == 0)
--                }
--                -- check(ok and m and (leftover==0), "m: " .. tostring(m) )
--            else
--                case_test_table[index]["\0"] = {
--                    ok = ok, m = m, leftover = leftover, check = ok and not m
--                }
--                -- check(ok and not m)
--            end
--
--            if (character_db[index]) then
--                for j, case in ipairs({"lower", "upper", "title"}) do
--                    if character_db[index][case] ~= "" then
--                        local case_char = utf8.char( tonumber(character_db[index][case], 16) )
--                        ok, m, leftover = e:match(p, case_char)
--                        case_test_table[index][case] = {
--                            ok = ok, m = m, leftover = leftover, check = ok and m and (leftover == 0)
--                        }
--                        -- check(ok, "ok: " .. tostring(ok) )
--                        -- check(m and (leftover==0),
--                        --      "char: " .. unicode_char .. ", " .. case .. " char: " .. case_char)
--                    end
--                end
--            end
--
--         else
--            print("compile failed: ")
--            table.print(errs, false)
--         end
--     end
--
--     local check_all_unicode_tests = true
--     for index = 0, max_codepoint do
--         for key, value in pairs(case_test_table[index]) do
--             -- print("key: " .. key)
--             local result = case_test_table[index][key].check
--             if not result then
--                 print("index " .. tostring(index)
--                     .. ", key " .. key .. ": "
--                     .. tostring(result)
--                 )
--             end
--
--
--             check_all_unicode_tests = check_all_unicode_tests and result
--         end
--     end
--
--     print("check_all_unicode_tests: " .. tostring(check_all_unicode_tests))
-- end
--
-- function print_case_mapping_file(optional_directory)
--     optional_directory = optional_directory or "/Users/Alec/Dropbox/Education/NCSU/Courses/mscs/CSC630/rosie/src/unicode/tmp"
--     local filename = "CaseMapping.bin"
--     local f, err = io.open(optional_directory .. "/" .. filename, 'rb')
--     if not f then error(err); end
--
--     for i = 1, 200 do -- 1114109
--         for j = 1, 4 do
--             local val = f:read(4)
--
--             for b in val:gmatch(".") do
--                 io.write(string.format("%02X", string.byte(b)))
--             end
--             io.write(" ")
--         end
--         print("\n")
--     end
--
-- end

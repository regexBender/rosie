-- -*- Mode: Lua; -*-
--
-- unicode.lua
--
-- © Copyright Jamie A. Jennings 2018.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings

local rosie = require("rosie")
import = rosie.import
local list = import("list")
local violation = import("violation")

local util = dofile("util.lua")

UNICODE_VERSION = "10.0.0"

UCD_DIR = assert(rosie.env.ROSIE_HOME) .. "/../../../src/unicode/UCD-" .. UNICODE_VERSION .. "/"
SAVE_FILES_IN_THIS_DIR = assert(rosie.env.ROSIE_HOME) .. "/../../../src/unicode"
CaseMappingFile = "CaseFolding.txt"


local engine = rosie.engine.new()
local ok, pkgname, errs = engine:loadfile("case-folding.rpl")
if not ok then
   error(table.concat(list.map(violation.tostring, errs), "\n"))
end

local function extract_values(data)
    assert(data.type == 'CaseFoldingLine')
    data = data.subs[1]
    if data.type=='comment' or data.type=='blank_line' then return; end
    assert(data.type=='CaseFoldingData')

    assert(data.subs[1].type=='codePoint')
    local code = data.subs[1].data -- one hex value
    assert(code)

    assert(data.subs[2].type=='status')
    local status = data.subs[2].data
    assert(    status == "C"
            or status == "F"
            or status == "S"
            or status == "T"
    )

    assert(data.subs[3].type=='mapping')
    local mapping = data.subs[3].data -- up to three space-delimited hex values
    assert(mapping)

    assert(data.subs[4].type=='character_name')
    local character_name = data.subs[4].data -- up to three space-delimited hex values
    assert(character_name)

    return {
        code           = code,
        status         = status,
        mapping        = mapping,
        character_name = character_name
    }
end


local function create_reverse_mapping_table(mapping_tables, name)

    local case_mappings = {n = 0}

    for _, mapping_table in ipairs(mapping_tables) do
        for case_variant_code, _ in pairs(mapping_table) do
            if case_variant_code ~= "n" then
                local case_variant_mappings = mapping_table[case_variant_code]

                for mapping_case_variant, _ in pairs(case_variant_mappings) do

                    -- add the reverse mapping
                    if case_mappings[mapping_case_variant] == nil then
                        case_mappings[mapping_case_variant] = {[case_variant_code] = 1}
                        case_mappings.n = case_mappings.n + 1
                    elseif case_mappings[mapping_case_variant][case_variant_code] ~= 1 and mapping_case_variant ~= case_variant_code then
                        case_mappings[mapping_case_variant][case_variant_code] = 1
                        case_mappings.n = case_mappings.n + 1
                    end

                end
            end
        end
    end

    optional_directory = optional_directory or SAVE_FILES_IN_THIS_DIR
    local filename = (optional_directory .. "/reverse-case-folding-table-" .. name .. ".lua")
    print("Writing " .. filename)

    util.print_table(case_mappings, filename)

    return case_mappings

end

local function create_two_way_mapping_table(mapping_tables, name)

    local case_mappings = {n = 0}

    for _, mapping_table in ipairs(mapping_tables) do
        for case_variant_code, _ in pairs(mapping_table) do
            if case_variant_code ~= "n" then
                local case_variant_mappings = mapping_table[case_variant_code]

                for mapping_case_variant, _ in pairs(case_variant_mappings) do

                    -- add the forward mapping
                    if case_mappings[case_variant_code] == nil then
                        case_mappings[case_variant_code] = {[mapping_case_variant] = 1}
                        case_mappings.n = case_mappings.n + 1
                    elseif case_mappings[case_variant_code][mapping_case_variant] ~= 1 and case_variant_code ~= mapping_case_variant then
                        case_mappings[case_variant_code][mapping_case_variant] = 1
                        case_mappings.n = case_mappings.n + 1
                    end

                    -- add the reverse mapping
                    if case_mappings[mapping_case_variant] == nil then
                        case_mappings[mapping_case_variant] = {[case_variant_code] = 1}
                        case_mappings.n = case_mappings.n + 1
                    elseif case_mappings[mapping_case_variant][case_variant_code] ~= 1 and mapping_case_variant ~= case_variant_code then
                        case_mappings[mapping_case_variant][case_variant_code] = 1
                        case_mappings.n = case_mappings.n + 1
                    end

                end
            end
        end
    end

    optional_directory = optional_directory or SAVE_FILES_IN_THIS_DIR
    local filename = (optional_directory .. "/two-way-case-folding-table-" .. name .. ".lua")
    print("Writing " .. filename)

    util.print_table(case_mappings, filename)

    return case_mappings

end


local function store_case_mapping_of_status(data, case_mappings, target_status)
    local mapping_data = extract_values(data)
    if mapping_data == nil or mapping_data.status ~= target_status then
        return false
    end

    -- table.print(mapping_data)
    -- case_mappings[mapping_data.code] = mapping_data.mapping
    if case_mappings[mapping_data.code] == nil then
        case_mappings[mapping_data.code] = {[mapping_data.mapping] = 1}
    elseif case_mappings[mapping_data.code][mapping_data.mapping] ~= 1 and mapping_data.code ~= mapping_data.mapping then
        case_mappings[code_case_variant][mapping_case_variant] = 1
    end
    case_mappings.n = case_mappings.n + 1

    return true

end

function load_case_mappings(target_status)
    local filename = CaseMappingFile

    print("Reading CaseFolding.txt")
    local case_mappings = {n = 0}

    local parser, errs = engine:compile("CaseFoldingLine")
    if not parser then
       error(table.concat(list.map(violation.tostring, errs), "\n"))
    end
    local nl = util.make_nextline_function(filename)
    local i = 1
    local line = nl()
    while(line) do
        local data = parser:match(line)
        if not data then
            print(string.format("Parse error at line %d: %q", i, line))
        elseif target_status == nil then
            store_all_case_mappings(data, case_mappings)
        else
            store_case_mapping_of_status(data, case_mappings, target_status)
        end
        line = nl();
        i = i+1;
    end

    print(tostring(case_mappings.n) .. " individual mappings processed")

    return case_mappings
end


function create_case_mapping_table(input_status)
    local status = input_status or "C"
    local mark = 4

    assert(    status == "C"
            or status == "F"
            or status == "S"
            or status == "T"
    )

    local case_folding_table = load_case_mappings(status)

    optional_directory = optional_directory or SAVE_FILES_IN_THIS_DIR
    local filename = (optional_directory .. "/case-folding-table-" .. status .. mark .. ".lua")
    print("Writing " .. filename)

    util.print_table(case_folding_table, filename)

    return case_folding_table
end

function create_full_case_folding_files()
    local C_mapping_table = create_case_mapping_table("C")
    local F_mapping_table = create_case_mapping_table("F")

    create_reverse_mapping_table({C_mapping_table, F_mapping_table}, "CF")
end


function create_simple_case_folding_files()
    local C_mapping_table = create_case_mapping_table("C")
    local F_mapping_table = create_case_mapping_table("S")

    create_reverse_mapping_table({C_mapping_table, F_mapping_table}, "CS")
end

-- -*- Mode: Lua; -*-                                                               
--
-- infix.lua    Convert infix expressions in parse tree to prefix
--
-- © Copyright Jamie A. Jennings 2019.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings

-- rpl_1_3 grammar avoids exponentially slow parsing of infix expressions by
-- parsing them as an infix sequence of interleaved operators and expressions,
-- which produces a tree like this:
--
--         EXP
--       /  | \      
--      /   |  \      
--     /    |   \
--  TERM OPERATOR EXP ...
--
-- The code below converts trees like this into the same form that the rpl_1_2
-- grammar produced, where the operator is at the root:
--
--     OPERATOR
--    /        \
--  TERM       EXP ...
--
-- We use Dijsktra's "shunting yard" algorithm, modified to:
-- (1) produce an AST (essentially, prefix notation, not postfix)
-- (2) recursively process each 'term' or 'exp' that contains 'infix'
-- (3) treat adjacency as an implicit sequence operator
-- Also, shunting yard does not need to handle parentheses.  Because
-- parenthetical expressions can be recognized by a unique prefix (an open
-- paren), the RPL grammar will parse one as a TERM.

local infix = {}

infix.DEBUG=false				    -- temporary

--assert(rosie, "'rosie' not defined.  Use this file in rosie lua, not plain lua.")
--import = rosie.import

local util = require("util")
local common = require("common")
local list = require("list")
local map = list.map
local filter = list.filter

local stack_mt =
   { __index = {push = function(self, elt)
			  self.top = self.top + 1
			  self[self.top]=elt
			  return
		       end,
		pop = function(self)
			 local top = self.top
			 local elt = self[top]
			 if top > 0 then self.top = top - 1; end
			 return elt
		      end,
		peek = function(self)
			  return self[self.top]
		       end,
		is_empty = function(self)
			      return self.top==0
			   end,
		to_list = function(self)
			     for i=self.top+1, #self do self[i]=nil; end
			     self.top = nil
			     return self
			  end}
  }
     
local function new_stack()
   local data = {}
   data.top = 0
   setmetatable(data, stack_mt)
   return data
end

local function is_atmosphere(pt)
   return not common.not_atmosphere(pt)
end

-- Find each exp that has a quantifier succeeding it, and attach the quantifier
-- to the exp.
local function attach_quantifiers(exps)
   if exps==nil then return nil end
   local i = 1
   local result = {}
   while i <= #exps do
      local item = exps[i]
      local succ = exps[i+1]
      if succ and succ.type=="quantifier" then
	 table.insert(result,
		      {type = "form.quantified_exp",
		       s = item.s,
		       e = item.e,
		       data = item.data .. succ.data,
		       subs = {item, succ.subs[1]}})
	 i = i + 1
      else
	 -- no successor, or it is not a quantifier
	 table.insert(result, item)
      end -- if successor is quantifier
      i = i + 1
   end -- while
   return result
end

-- Find two exps in a row with no operator between, and insert an explicit
-- sequence operator.  Assumes quantifiers have already been attached.
local function insert_seq_operators(exps)
   if exps==nil then return nil end
   local result = {}
   local item, last_seen
   local i = 1
   while i <= #exps do
      item = exps[i]
      if is_atmosphere(item) or item.type=="complement" then
	 -- no op, just enqueue it into the result list
      elseif item.type=="operator" then
	 --!assert( last_seen )
	 last_seen = "OP"
	 -- enqueue it into the result list
      else
	 -- rpl expression
	 if last_seen and (last_seen == "EXP") then
	    table.insert(result,
			 {type = "operator",
			  s = result[#result].e,
			  e = item.s - 1,
			  data = "SEQUENCE"})
	 end -- if need to insert operator
	 last_seen = "EXP"
      end
      table.insert(result, item)
      i = i + 1
   end -- while
   return result
end

local shunting_yard_explist;
local handle_statement;

local function shunting_yard_exp(exp, attribute_table)
   if infix.DEBUG then print("infix.DEBUG: entering shunting_yard_exp with ", exp.type, " ", exp.data) end
   local function map_shunting_yard_exp(ls)
      return map(function(e) return shunting_yard_exp(e, attribute_table) end, ls)
   end
   if exp.type == "form.exp" or
      exp.type == "form.term" or
      exp.type == "form.bracket" then
      return {type=exp.type,
	      s=exp.s,
	      e=exp.e,
	      data=exp.data,
	      subs=shunting_yard_explist(exp.subs)}
   elseif exp.type == "form.quantified_exp" then
      return {type=exp.type,
	      s=exp.s,
	      e=exp.e,
	      data=exp.data,
	      subs={shunting_yard_exp(exp.subs[1]),
		    exp.subs[2]}}
   elseif exp.type == "form.cooked" or
      exp.type == "form.raw" then
      return {type=exp.type,
	      s=exp.s,
	      e=exp.e,
	      data=exp.data,
	      subs=map_shunting_yard_exp(exp.subs)}
   elseif exp.type == "form.grammar_exp" or exp.type == "form.let_exp" then
      --    grammar_exp = "grammar" identifier atmos (bindings "in")? exp
      --    let_exp = "let" atmos (bindings "in")? exp
      local expression = exp.subs[#exp.subs]
      local bindings = filter(function(sub)
				 return sub.type=="bindings"
			      end,
			      exp.subs)
      local newbindings
      if #bindings==0 then
	 newbindings = nil
      else
	 newbindings = map(handle_statement, bindings[1].subs)
      end
      local newbindingsub = {type=bindings.type,
			     s=bindings.s,
			     e=bindings.e,
			     data=bindings.data,
			     subs=newbindings}
      local newsubs = {}
      for i, sub in ipairs(exp.subs) do
	 if sub.type=="bindings" then
	    newsubs[i] = newbindingsub
	 else
	    newsubs[i] = sub;
	 end
      end
      newsubs[#newsubs] = shunting_yard_exp(expression)
      return {type=exp.type,
	      s=exp.s,
	      e=exp.e,
	      data=exp.data,
	      subs=newsubs}
   elseif exp.type == "form.application" then
      local n = #exp.subs
      local arg_form = exp.subs[n]
      local newsubs = {}
      for i=1,n-1 do newsubs[i] = exp.subs[i] end
      newsubs[n] = {type=arg_form.type,
		    s=arg_form.s,
		    e=arg_form.e,
		    data=arg_form.data,
		    subs=map_shunting_yard_exp(arg_form.subs)}
      return {type=exp.type,
	      s=exp.s,
	      e=exp.e,
	      data=exp.data,
	      subs=newsubs}
   elseif exp.type == "form.predicate" then
      local n = #exp.subs
      local exp_form = exp.subs[n]
      local newsubs = {}
      for i=1,n-1 do newsubs[i] = exp.subs[i] end
      newsubs[n] = shunting_yard_exp(exp_form)
      return {type=exp.type,
	      s=exp.s,
	      e=exp.e,
	      data=exp.data,
	      subs=newsubs}
   else
      if infix.DEBUG then print("infix.DEBUG: skipping ", exp.type, " ", exp.data) end
      return exp
   end
end

-------------------------------------------------------------------------------
-- Note: RPL is right associative and the operators have equal precedence. As a
-- result, we do NOT need a table of operator attributes, because should_push()
-- can always return true.
--
-- We did a full implementation, including handling operators of different
-- precedence and associativity, for one reason: In the future, someone may want
-- to copy this code into a new output encoder that performs shunting yard.
--
-- That code would presumably be parameterized by knowledge of what RPL type to
-- look for in the Rosie output ("form.exp" in our case here), how to recognize
-- operators (type=="operator" here), and how to walk an expression looking for
-- more expressions to process.
--
-- The latter function, which walks an expression tree looking for more
-- expressions, is implemented by shunting_yard_exp() here.

local OPERATOR_ATTRIBUTES =
   { ["SEQUENCE"] = { associativity="right", precedence=10, nodetype="form.sequence" },
     ["/"]        = { associativity="right", precedence=10, nodetype="form.choice"},
     ["&"]        = { associativity="right", precedence=10, nodetype="form.and_exp"} }

local function should_push(op, stack_top, attribute_table)
   local attr = attribute_table[op]
   if not stack_top then
      -- stack is empty
      return true
   else
      local stack_op_attr = attribute_table[stack_top.data]
      local op_precedence = attr.precedence
      local stack_op_precedence = stack_op_attr.precedence
      if (op_precedence > stack_op_precedence) or
	 ( (op_precedence == stack_op_precedence) and
	   (attr.associativity == "right") ) then
	 return true
      else
	 -- incoming op either has lower precedence as the one on top of stack,
	 -- or has equal precedence but is left associative
	 return false
      end
   end
   error("should not get here")
end

-- The stack of expressions in the original shunting yard algorithm contains
-- only expressions.  In Rosie, it also contains items of atmosphere,
-- i.e. comments and newlines.  We will collect those that come after an
-- expression and associate them with that expression.
local function pop_exp(exps)
   local items = {}
   while (not exps:is_empty()) do
      local item = exps:pop()
      table.insert(items, item)
      if not is_atmosphere(item) then break; end
   end
   return items
end

local function show(label, ls)
   io.stdout:write(label, ": ")
   for k,v in ipairs(ls) do
      io.stdout:write(v.type)
   end
   io.stdout:write("\n")
end

local function make_prefix_tree(op, exps)
   local right = pop_exp(exps)
   local left = pop_exp(exps)
   if infix.DEBUG then
      show("right operand: ", right)
      show("left operand: ", left)
   end
   --!assert(right[1], "missing right operand for: " .. op.data)
   --!assert(left[1],  "missing left operand for: " .. op.data)
   op.subs = list.append(left, right)
   -- Convert generic "operator" nodes to choice, sequence, or and_exp
   local attr = OPERATOR_ATTRIBUTES[op.data]
   op.type = attr.nodetype
   if infix.DEBUG then
      print("result: ", op.type); --table.print(op)
   end
   return op
end

--------------------------------------------------------------------------------
-- This is the shunting yard algorithm implementation, shunting_yard_explist().
-- Its first argument is a list containing expressions and operators.  It is
-- called by shunting_yard_exp(), which understands the structure of RPL
-- expressions, i.e. it knows where to find infix expressions.

function shunting_yard_explist(subs, attribute_table)
   if not attribute_table then attribute_table = OPERATOR_ATTRIBUTES end
   if infix.DEBUG then
      io.stdout:write("  entering: ")
      for _,v in ipairs(subs) do io.stdout:write(v.type, " ") end
      print()
   end
   subs = attach_quantifiers(subs)
   if infix.DEBUG then
      io.stdout:write("  after attaching quantifiers: ")
      for _,v in ipairs(subs) do io.stdout:write(v.type, " ") end
      print()
   end
   subs = insert_seq_operators(subs)
   if infix.DEBUG then
      io.stdout:write("  after inserting seq operators: ")
      for _,v in ipairs(subs) do io.stdout:write(v.type, " ") end
      print()
   end
   local exps = new_stack()
   local opstack = new_stack()
   for _, e in ipairs(subs) do
      if is_atmosphere(e) then
	 exps:push(e)
      elseif e.type == "operator" then
	 while not should_push(e.data, opstack:peek(), attribute_table) do
	    -- Convert to prefix in tree form. 
	    exps:push(make_prefix_tree(opstack:pop(), exps))
	    -- To produce a postfix sequence of exps and ops, as the original
	    -- shunting yard algorithm did, do this instead of the above:
	    -- exps:push(opstack:pop())
	 end
	 opstack:push(e)	    
      else
	 exps:push(shunting_yard_exp(e, attribute_table))
      end
   end
   while not opstack:is_empty() do
      -- To produce a postfix sequence of exps and ops:
      -- exps:push(opstack:pop())
      exps:push(make_prefix_tree(opstack:pop(), exps))
   end
   return exps:to_list()
end

function shunting_yard(exp)
   if exp.type=="form.exp" then
      return shunting_yard_exp(exp)
   else
      return exp
   end
end

--------------------------------------------------------------------------------
-- handle_statement() dispatches based on the kind of statement.  Bindings have
-- several forms in RPL.  A simple binding has an identifier and an expression,
-- and we call shunting_yard on the expression.  Other binding forms are more
-- complicated, so we have to take them apart to find the simple bindings inside
-- them. 

function handle_statement(s)
   if s.type=="form.binding" then
      return {type=s.type,
	      s=s.s,
	      e=s.e,
	      data=s.data,
	      subs = map(handle_statement, s.subs)}
   elseif s.type=="form.empty" then
      return s
   elseif s.type=="form.simple" then
      local newsubs = {}
      local n = #s.subs
      --!assert( s.subs[n].type=="form.exp" )
      for i=1,n-1 do newsubs[i] = s.subs[i] end
      newsubs[n] = shunting_yard(s.subs[n])
      return {type=s.type,
	      s=s.s,
	      e=s.e,
	      data=s.data,
	      subs = newsubs}
   elseif s.type=="form.grammar_block"
      or s.type=="form.let_block" then
      --!assert(s.subs[1].type=="form.bindings")
      -- there may be one set of bindings or two
      local newsubs = {}
      for _, bindings in ipairs(s.subs) do
	 local pt = {type=bindings.type,
		      s=bindings.s,
		      e=bindings.e,
		      data=bindings.data,
		      subs=bindings.subs and map(handle_statement, bindings.subs)}
	 table.insert(newsubs, pt)
      end -- for
      return {type=s.type,
	      s=s.s,
	      e=s.e,
	      data=s.data,
	      subs = newsubs}
   elseif s.type=="package_decl" or 
      s.type=="import_decl" or
      s.type=="language_decl" or
      is_atmosphere(s) then
      return s
   elseif s.type=="form.exp" then
      -- Oddly, perhaps, 'rpl_statements' can match 'form.exp'.  This is
      -- intentional, and allows a better error message to be issued by code
      -- that wants a statment but gets an expression.  Similarly,
      -- 'rpl_expression' can match a binding for the same reason.
      return {type = s.type,
	      s = s.s,
	      e = s.e,
	      data = s.data,
	      subs = map(shunting_yard, s.subs)}
   else
      error("unexpected statement type: " .. tostring(s.type))
   end
end

--------------------------------------------------------------------------------
-- Main entry point:
-- 
-- The RPL grammar (in rpl_1_3.rpl) defines two patterns for parsing RPL, one
-- for expressions and one for statements.  This entry point dispatches based on
-- whether the incoming parse tree, pt, is one or the other.

function infix.to_prefix(pt)
   if pt.type=="rpl_expression" then
      --!assert(pt.subs)
      return {type = pt.type,
	      s = pt.s,
	      e = pt.e,
	      data = pt.data,
	      subs = map(shunting_yard, pt.subs)}
   elseif pt.type=="rpl_statements" then
      -- There can be no subs, which happens if some component of rosie tries to
      -- parse a statement and gets only a language declaration, which gets
      -- stripped out in pre-parsing, leaving no actual statements.
      local newsubs = (pt.subs and map(handle_statement, pt.subs)) or nil
      return {type = pt.type,
	      s = pt.s,
	      e = pt.e,
	      data = pt.data,
	      subs = newsubs}
   else
      error("unexpected parse tree type: " .. tostring(pt.type))
   end
end

return infix

--------------------------------------------------------------------------------
-- FOR TESTING DURING DEVELOPMENT

--[==[

function slurp(filename)
   local f, err = io.open(filename)
   if not f then error(err) end
   local txt = f:read("a")
   f:close()
   return txt
end

function indent(str, col)
   return str:gsub('\n', '\n' .. string.rep(" ", col))
end

right_column = 4

function run_unittests(test_engine, test_rplx, filename, pkgname)
   io.stdout:write(filename, '\n')

   local test_lines = unittest.extract_tests_from_file(filename)
   if #test_lines == 0 then
      unittest.write_test_result("No tests found")
      return true
   end
   local failures, errors, blocked, passed, total = 0, 0, 0, 0, 0
   for _, line in pairs(test_lines) do
      local ee, ff, bb, pp, tt, msgs = unittest.do_line(test_rplx, line, test_engine, pkgname)
      errors = errors + ee
      failures = failures + ff
      blocked = blocked + bb
      passed = passed + pp
      total = total + tt
      for _, msg in ipairs(msgs) do unittest.write_test_result(msg) end
   end -- for each line of tests
   if failures == 0 and errors == 0 and blocked == 0 then
      unittest.write_test_result("All ", tostring(total), " tests passed")
   else
      unittest.write_test_result("Tests: ", tostring(total),
				 "  Errors: ", tostring(errors),
				 "  Failures: ", tostring(failures),
				 "  Blocked: ", tostring(blocked),
				 "  Passed: ", tostring(passed))
   end
   return true, errors, failures, blocked, passed, total
end      
      
function test1(filename)
   -- NOTE: Using globals here, for ease of debugging
   assert(test_rplx, "no test_rplx.  maybe setup() not run?")
   io.stdout:write("Testing ", filename, "\n")
   io.stdout:flush()

   src = slurp(filename)

   orig = rosie.engine.new()
   cli_common.setup_engine(rosie, orig, {norcfile=true})
   collectgarbage('collect'); collectgarbage('collect')
   t0 = os.clock()
   ok, pkgname, err = orig:load(src)
   assert(ok and pkgname)
   t1 = os.clock()
   old_time = t1 - t0
   unittest.write_test_result(string.format("OLD load time %2.4f seconds", old_time))
   
   e = new_engine()
   cli_common.setup_engine(rosie, e, {norcfile=true})

   collectgarbage('collect'); collectgarbage('collect')
   t0 = os.clock()
   ok, pkgname, err = e:load(src)
   t1 = os.clock()
   new_time = t1 - t0
   unittest.write_test_result(
      string.format("New load time %2.4f seconds, %3.0f%% change",
		    new_time,
		    ((new_time - old_time) * 100) / old_time))
   if ok then
      if pkgname then
	 unittest.write_test_result("OK (loaded " .. pkgname .. ")")
      else
	 unittest.write_test_result("OK")
      end
   else
      error("load failed.  inspect 'err' for details.")
   end
   ok, ee, ff, bb, pp, tt =
      run_unittests(e, test_rplx, filename, pkgname)
   assert(ok)
end


function test()
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/all.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/char.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/csv.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/date.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/json.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/net.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/num.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/os.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/re.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/time.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/ver.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/word.rpl")

   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/Ascii.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/Block.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/Category.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/GraphemeBreak.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/LineBreak.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/NumericType.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/Property.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/Script.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/SentenceBreak.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/Unicode/WordBreak.rpl")

   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/rosie/rcfile.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/rosie/rpl_1_1.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/rosie/rpl_1_2.rpl")
   test1("/Users/jennings/Projects/rosie-pattern-language/dev/rpl/rosie/rpl_1_3.rpl")

end

function new_engine()
   local e = rosie.engine.new()
   parse_block = rosie.env.compile.make_parse_block(preparse, stmt, e.compiler.version, infix.to_prefix)
   parse_expression = rosie.env.compile.make_parse_expression(exp, infix.to_prefix)
   e.compiler.parse_block = parse_block
   e.compiler.parse_expression = parse_expression
   e.compiler.version = common.rpl_version.new(1,3)
   return e
end

function setup()
   rpl_definition_file = "rosie/rpl_1_3"
   oe = rosie.engine.new()
   assert((oe:import(rpl_definition_file, '.')))
   exp = assert((oe:compile('rpl_expression')))
   stmt = assert((oe:compile('rpl_statements')))
   preparse = assert((oe:compile('preparse')))

   unittest = import('unittest')
   test_rplx = unittest.setup(oe, rpl_definition_file)
   assert(test_rplx)
end

return infix


-- Demonstration of how the infix grammar in rpl_1_3 (combined with shunting
-- yard) eliminates the exponential behavior of the original grammar from
-- rpl_1_2:
--    e is an engine set up with rpl_1_3 and shunting yard
--    orig is a plain rpl_1_2 engine
-- 
-- Performance is roughly equal for no parens and 2 parens, and significantly
-- better when there are more.
-- 
-- > -- 6 sets of parens
-- > collectgarbage('collect'); t0=os.clock(); for i=1,10000 do x = e:compile('(((((("a"))))))'); end; t1=os.clock(); print((t1-t0)/10)
-- 0.4165246
-- > collectgarbage('collect'); t0=os.clock(); for i=1,10000 do x = orig:compile('(((((("a"))))))'); end; t1=os.clock(); print((t1-t0)/10)
-- 3.097605
-- > 
-- > -- 4 sets of parens
-- > collectgarbage('collect'); t0=os.clock(); for i=1,10000 do x = e:compile('(((("a"))))'); end; t1=os.clock(); print((t1-t0)/10)
-- 0.3444756
-- > collectgarbage('collect'); t0=os.clock(); for i=1,10000 do x = orig:compile('(((("a"))))'); end; t1=os.clock(); print((t1-t0)/10)
-- 0.5154367
-- > 
-- > -- 2 sets of parens
-- > collectgarbage('collect'); t0=os.clock(); for i=1,10000 do x = e:compile('(("a"))'); end; t1=os.clock(); print((t1-t0)/10)
-- 0.2667446
-- > collectgarbage('collect'); t0=os.clock(); for i=1,10000 do x = orig:compile('(("a"))'); end; t1=os.clock(); print((t1-t0)/10)
-- 0.2808682
-- > 
-- > -- 0 sets of parens
-- > collectgarbage('collect'); t0=os.clock(); for i=1,10000 do x = e:compile('"a"'); end; t1=os.clock(); print((t1-t0)/10)
-- 0.181641
-- > collectgarbage('collect'); t0=os.clock(); for i=1,10000 do x = orig:compile('"a"'); end; t1=os.clock(); print((t1-t0)/10)
-- 0.1787887
-- >

--]==]

-- -*- Mode: Lua; -*-                                                               
--
-- prepare.lua   Used by all.lua and the test/run script
--
-- © Copyright Jamie A. Jennings 2020.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings

if not ROSIE then
   print("*****************************************************************")
   print("** Path to rosie executable not set.  Some tests will not run. **")
   print("*****************************************************************")
end

import = rosie.import

ROSIE_HOME = rosie.env.ROSIE_HOME
rosie_cmd = ROSIE

json = import "cjson"

package.path = "./submodules/lua-modules/?.lua"
termcolor = assert(require("termcolor"))
test = assert(require("test"))

return

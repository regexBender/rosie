[![current version 1.2.2](https://img.shields.io/badge/version-1.2.2-093073)](./CHANGELOG)
[![pipeline status](https://gitlab.com/rosie-pattern-language/rosie/badges/master/pipeline.svg)](https://gitlab.com/rosie-pattern-language/rosie/commits/master)

# Rosie Pattern Language (RPL)

## News
* v1.2.2 released (February 2021)
* v1.2.2-beta released (May 2020)
* v1.2.1 released (October 2019)
* [Brew installer for Mac OS X](https://gitlab.com/rosie-community/packages/homebrew-rosie)
* Pip installer for Python interface to librosie -- `pip install rosie`

## What is the Rosie Pattern Language?

The Rosie Pattern Language (RPL) is intended to replace regular expressions
(regex) in situations where: 

- **many regex** are used in an application, project, or organization; or
- some regex are used by **many people**, or by a few people over a long period of time; or
- regex are used in **production** systems, where the cost of run-time errors is high.

The advantages conferred by RPL in cases 1 and 2 derive from the RPL syntax and
expressive power.  The syntax resembles a programming language, making
expressions easier to read and understand, and compatible with tools like
`diff`.  

RPL is based on
[Parsing Expression Grammars](https://en.wikipedia.org/wiki/Parsing_expression_grammar),
which are more powerful than regex, obviating the need for "recursive regex" or
"regex grammars", both of which are ad hoc extensions and are not commonly
supported in regex libraries.

All three situations listed above involve **maintainability**.  RPL is easier
to maintain than regex (which are considered "write only" by most developers).
The Rosie project provides both a command line and REPL for development and
debugging.

Also, RPL supports executable unit tests, making it possible to:

1. have a suite of regression tests
2. test expressions independent of the code that uses them
3. compile and test expressions at build time, avoiding run-time errors in production

## What is the Rosie project?

Rosie is an implementation of RPL that is designed to scale to big data, many
developers, and large collections of patterns.

The Rosie project provides a CLI (like Unix `grep`) as well as a library,
`librosie`, that can be used within programs written in Python, Go, Haskell, C,
and other languages.

In the screen capture below, the `net.url` pattern (from the `net` library) is
used to extract all of the URLs mentioned on the google home page.

![](extra/examples/images/readme-fig3.png)

Suppose we wanted to see all the sub-domains of *google.com* that are
referenced on google's home page.  The `net.fqdn` pattern matches domain names,
and the _look behind_ operator `<` can be used to look backwards to see if the
matched domain ended in `google.com`.  (In RPL, to match a literal string, you
place it in double quotes, like string literals in other programs.)

![](extra/examples/images/readme-fig4.png)

## Documentation

* See the [documentation index](doc/README.md)
* And some [examples](extra/examples/README.md)
* And some other [extras](extra), like syntax highlighting for vim and Emacs
* There is also a set of [Rosie Community](https://gitlab.com/rosie-community/)
  repositories, with interfaces to `librosie` for various programming languages,
  collections of patterns, and syntax highlighting for other editors

## Why RPL?

* More **readable** and **maintainable** than regex
  * RPL [looks like a programming language](rpl/num.rpl), with whitespace, comments, identifiers
  * Built-in unit test framework with `rosie test` (useful as regression tests when modifying patterns)
  * Plays well with development tools (like `diff`, ci tools)
  * Patterns can be (optionally) put into namespaces for easy sharing, e.g. `net` for network patterns
* Better **development experience** than regex
  * Rosie ships with a library of dozens of useful patterns (timestamps, network addresses, and more)
  * Want to see how a match succeeds or fails? Use the `rosie trace` command.
  * Pattern development tools: tracing, REPL, color-coded match output
  * The `rosie test` command compiles patterns and runs their unit tests.  Use
    this command during your project's build to avoid run-time errors.
* Rosie produces **match output in multiple formats** including:
  * Color, for human-readable use at the command line
  * Plain text (full text `-o data` or list of sub-matches `-o subs`) for scripting
  * JSON to use as input to other programs
  * Native data structures in Python, Haskell, Go, etc.
* RPL is a **Parsing Expression Grammar** (PEG) language
  * A superset of regular expressions, i.e. more powerful
  * Allows recursive grammars, so RPL can recognize recursively defined
    structures like [JSON](rpl/json.rpl)
  * The PEG formalism is an elegant alternative to the ad hoc
    extensions to regular expressions found in most "regex" libraries
  * Supports linear run-time (in the input size) for common use cases

A note on comments and whitespace in regex: Some regex libraries support the
switch `\\x` which permits both comments and whitespace inside the regex.  We
hope this capability will be more widely adopted, because it's very useful!  Of
course, an "external DSL" for regex with development tooling and a packaging
standard would also be helpful.  And that is why we made RPL, to go after all of
these goals.


## Features

- Reasonably small:
  * The Rosie compiler/runtime/libraries take up less than 2MB on disk. <!-- 
  du -ch /usr/local/lib/librosie.a /usr/local/bin/rosie /usr/local/lib/rosie/lib /usr/local/lib/rosie/rpl 
  -->
  * See a [discussion of Rosie performance](doc/performance.md).
- Reasonably good performance: 
  * Faster than [Grok](https://www.elastic.co/guide/en/logstash/current/plugins-filters-grok.html)
  (by a factor of 4 or more).
  * Slower than Unix [grep](https://en.wikipedia.org/wiki/Grep).
  * But Rosie does more than both of them -- and the CLI is still single-threaded.
  * See a [discussion of Rosie performance](doc/performance.md).
- Extensible pattern library
- Pattern development tools: CLI, REPL, unit tests, trace facility
- Rosie is fluent in Unicode (UTF-8), ASCII, and the
  [binary language of moisture vaporators](http://www.starwars.com/databank/moisture-vaporator)
  (arbitrary byte-encoded data).


## Installing

- [x] Build from source (see [local installation](#local-installation) below)
- [X] [Brew installer for Mac OS X](https://gitlab.com/rosie-community/packages/homebrew-rosie)
- [ ] RPM and debian packages (help wanted!)

### Repository organization

Releases are tagged, e.g. `v1.2.0`.  The head of the master branch is always the
latest release plus any post-release documentation updates (which will be folded
into the next release).

The dev branch should be a stable development build, possibly with some quirks
and likely with out-of-date documentation.  If you are a contributor, you
probably want to be running the latest dev branch.

### Build dependencies

Tools: git, make, gcc/cc <br>
Libraries: readline, readline-devel <br>
Additional libraries for Linux: libbsd, libbsd-devel

### Supported platforms

Platforms: (most of these were tested with docker)

- [x] [Arch Linux](https://www.archlinux.org/) 
- [x] [CentOS Linux](https://www.centos.org)
- [x] [ElementaryOS Linux](https://elementary.io)
- [x] [Fedora Linux](https://getfedora.org)
- [x] OS X (macOS 10.13 and up)
- [x] [Ubuntu](https://www.ubuntu.com/)
- [x] Windows Subsystem for Linux (see [example installation script](extra/WSL/rosie_install.sh))
- [ ] Windows (Help wanted!)

### Local installation

To install Rosie, clone this repository and `cd rosie-pattern-language` (the
_build directory_).  Then run `make`.

If the build succeeds, you will see a message like this:

```
Rosie Pattern Engine 1.2.0 built successfully!
    Use 'make install' to install into DESTDIR=/usr/local
    Use 'make uninstall' to uninstall from DESTDIR=/usr/local
    To run rosie from the build directory, use ./bin/rosie
    Try this example, and look for color text output: rosie match all.things test/resolv.conf
``` 

If the build fails, you probably need to install one of the dependencies listed
above, using a package management command like `apt-get`, `dnf install`, or
`brew install`.  More information on building Rosie are [here](doc/deployment.md).

After a successful build, you can run the Rosie CLI from the build directory
with `bin/rosie`.  Try the example suggested in the build message:

```
bin/rosie match all.things test/resolv.conf
```

The output is a color rendering of the file `test/resolv.conf` in which each
color indicates that a particular pattern matched.  The bold blue font, for
example, is used for strings that match the pattern `time.any`.

Try searching (using `rosie grep`) for lines containing this pattern:

```
bin/rosie grep -o color time.any test/resolv.conf 
```

The `-o color` option sets the output mode to color.  Otherwise, the `rosie
grep` command behaves like Unix `grep` and simply prints all matching lines.

Searching for literal strings requires double quotes. And since the shell will
strip off the double quotes, we have to put the entire RPL pattern in single quotes:

```
bin/rosie grep -o color '"domain"' test/resolv.conf 
```

Finally, let's search for lines that start with the word "domain" followed by a
network address.  We can use `rosie match` instead of `rosie grep` because we
want matching to start at the beginning of the line:

```
bin/rosie match '"domain" net.any' test/resolv.conf 
```

The default output format for `rosie match` is color.  To see JSON output, try
any of the commands above with `-o jsonpp` (for JSON pretty-printed) or `-o
json`. 


### System installation

Running `make install` creates an installation directory in `/usr/local` (by
default, or in DESTDIR otherwise).  The executable is `/usr/local/bin/rosie`.
Other files used by Rosie can be found in `/usr/local/lib/rosie/`.


## Other sources of information

Rosie announcements on Twitter:
* [@jamietheriveter](https://twitter.com/jamietheriveter)

Rosie's home page, with blog posts and other info:
* [Rosie home page](http:tiny.cc/rosie)

Rosie on IBM developerWorks Open: _(N.B. Examples may be out of date.)_
* [Rosie blogs and talks](https://developer.ibm.com/code/category/rosie-pattern-language/)
* Including:
    * [Project Overview](https://developer.ibm.com/code/rosie-pattern-language/)
    * [Introduction](https://developer.ibm.com/code/2016/02/20/world-data-science-needs-rosie-pattern-language/)
    * [Parsing Spark logs](https://developer.ibm.com/code/2016/04/26/develop-test-rosie-pattern-language-patterns-part-1-parsing-log-files/)
    * [Parsing CSV files](https://developer.ibm.com/code/2016/10/14/develop-test-rosie-pattern-language-patterns-part-2-csv-data/)

For an introduction to Rosie and explanations of the key concepts, see
[Rosie's _raison d'etre_](doc/raisondetre.md).

I wrote some [notes](doc/geek.md) on Rosie's design and theoretical foundation
for my fellow PL and CS enthusiasts.

## Project roadmap

### Release History
- [x] v1.1.0 (2019 March)
- [x] v1.0.0 (2018 June)
- [x] v1.0.0-beta (2018 February)
- [x] v1.0.0-alpha (2017 September)

### API and language support
- [x] C API (`librosie`)
- [x] Python module (`rosie.py`)
- [x] C client of `librosie.so` (dynamic library)
- [x] C client of `librosie.o` (static linking)
- [x] Go module
- [x] Haskell module
- [ ] Ruby, node.js modules

### Roadmap (future)
- [ ] Ahead-of-time compilation
- [ ] Support JSON output for trace, config, list, and other commands
- [ ] Generate patterns automatically from locale data
- [ ] Linter
- [ ] Toolkit for user-developed macros
- [ ] Toolkit for user-developed output encoders
- [ ] Compiler optimizations

<hr>

## Contributing

### Write new patterns!

We are interesting in adding more patterns to the "standard" library that ships
with Rosie in the [rpl directory](rpl).  And we have a group of Community
repositories for RPL patterns where you can be a maintainer of your own RPL
packages.  

Of course, you can also publish your own RPL patterns, hosted wherever you keep
other code online.  In that case, let us know about it and we'll link to it!

### Calling Rosie from Go, C, Python, Haskell, ...  <a name="api-help"></a> 

Rosie is available as a [C library](src/librosie/librosie.c) that is compiled
both as a static and dynamic (shared) library.  An assortment of interface
libraries (or "clients") are hosted in
[their own repositories](https://gitlab.com/rosie-community/clients/). We are
eager to get pull requests for improvements (some of the clients are first
drafts) and for interfaces to other languages.

Before Rosie v1.0, we had sample interfaces to node.js and Ruby as well.
These have not been updated, and there were many changes to `librosie` when v1.0
was released.  But we are confident that interfaces to those languages can be
created in a reasonably straightforward way.  Please open an issue to request a
particular language interface or to offer to work on one!

### Wanted: RPL tools

Because RPL is designed like a programming language (and it has an accessible
parser, [rpl_1_2.rpl](rpl/rosie/rpl_1_2.rpl), new tools are relatively easy to
write.  Here are some ideas:

- **Package doc:** Given a package name, display the exported pattern names
      and, for each, a summary of the strings accepted and rejected.

- **Improved trace:** The current trace output could be improved,
  particularly to make it more compact.  A trace is represented internally as a
  table which could easily be rendered as JSON.  And since this data structure
  represents a complete trace, it is the right input to a new algorithm that
  produces a compact summary.  Or an animated output.

- **Linter:** Users of most programming languages are aided by a linting
	tool, in part because of correct expressions that are not, in fact, what the
	programmer wanted.  For example, the character set `[_-.]` is a range in
	RPL, but it is an empty range.  Probably the author meant to write a set of
	3 characters, like `[._-]`.

- **Notebook:** A Rosie kernel for a notebook would be useful to many
  people.  So would adding Rosie capabilities to a general notebook environment
  (e.g. [Jupyter](http://jupyter.org)).
  
- **Pattern generators:** A number of techniques hold promise for
automatically generating RPL patterns, for example:
  * Convert a format string to pattern, e.g. a `printf` format string, or the
    posix locale structure's fields that specify how to format numbers,
    dates/times, and monetary amounts.
  * Infer the format of each field in a CSV (or JSON, HTML, XML) file using
  analytics techniques such as statistics and machine learning.
  * Convert a regular expression to an RPL pattern.



## Acknowledgements

In addition to the people listed in the CONTRIBUTORS file, we wish to thank:

- Roberto Ierusalimschy, Waldemar Celes, and Luiz Henrique de Figueiredo, the
  creators of [the Lua language](http://www.lua.org) (MIT License); and again
  Roberto, for his [lpeg library](http://www.inf.puc-rio.br/~roberto/lpeg) (MIT
  License), which has been critical to implementing Rosie.

-  The Lua community (at large);

-  Mark Pulford, the author of
   [lua-cjson](http://www.kyne.com.au/%7Emark/software/lua-cjson.php) (MIT
   License); 

-  Brian Nash, the author of
   [lua-readline](https://github.com/bcnjr5/lua-readline) (MIT License); 

-  Peter Melnichenko, the author of
   [argparse](https://github.com/mpeterv/argparse) (MIT License);

